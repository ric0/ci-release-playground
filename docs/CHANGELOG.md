# [1.4.0](https://gitlab.com/ric0/ci-release-playground/compare/v1.3.0...v1.4.0) (2023-10-28)


### Features

* add @semantic-release/changelog ([567ca74](https://gitlab.com/ric0/ci-release-playground/commit/567ca7440cc58ab726dc530ccf7617bfa67ab25e))
