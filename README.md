# CI Reelase Playground

Purpose of this repo is to experiment with release automation.

## Tools

[semantic-release](https://github.com/semantic-release/semantic-release)

Local execution command:

    npx semantic-release --no-ci 

Protocol:

- run `npm init` to create `package.json`
- run `npm install --save-dev semantic-release`
- create a Personal Access Token for Gitlab, set as env `GITLAB_TOKEN`
- create `.releaserc.js` with minimal config (branches, plugins)
- run tool -> it created a tag and pushed it automatically, nothing else

- add @semantic-release/gitlab with default config
- run tool -> a new Release is created on Gitlab (including Changelog and an archive of files!)

- rerun tool -> no new commits detected since last release -> release is skipped
- create commit with message `docs: ...`
- rerun tool -> "There are no relevant changes, so no new version is released."

- add @semantic-release/npm with npmPublish disabled
- run tool -> gitlab-release is created, tag is pushed, ! but version in package.json is not commited, only modified locally

- add @semantic-release/git with config to commit package.json, package-lock.json
- run tool -> the new version in package.json is now commited and automatically pushed

- add @semantic-release/changelog with specific location (docs-folder)
- run tool -> changelog is generated and successfully pushed to repo

Tasks (unorderd):

- [x] Run locally
- [x] Automatically detect and generate next version (git tag)
- [x] Create Tag automatically
- [x] Update version in package.json
- [x] Create Release in Gitlab
- [ ] Run in CI-Pipeline
- [ ] Trigger release in CI dynamically (release-branch, CI-Flag)
- [ ] Evaluate mutlibranch setup

---

[release-it](https://github.com/release-it/release-it)

Cons: It does not support the creation of Pull Requests.
